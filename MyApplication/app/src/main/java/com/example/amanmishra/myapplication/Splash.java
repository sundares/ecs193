package com.example.amanmishra.myapplication;

/**
 * Created by Aman Mishra on 2/6/2016.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import java.util.logging.Handler;

public class Splash extends AppCompatActivity{
    private static int splashInterval = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_screen);
        Thread myThread = new Thread(){
            @Override
            public void run(){
                try{
                    sleep(3000);
                    if(SaveSharedPreference.getUserName(Splash.this).length() == 0)
                    {
                        Intent startMainScreen = new Intent(getApplicationContext(), MainPage.class);
                        startActivity(startMainScreen);
                        finish();
                    }
                    else
                    {
                        Intent startHomeScreen = new Intent(getApplicationContext(), HomeScreen.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("email", SaveSharedPreference.getUserName(Splash.this));
                        startHomeScreen.putExtras(bundle);
                        startActivity(startHomeScreen);
                        finish();
                    }
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        };
        myThread.start();

    }
}
