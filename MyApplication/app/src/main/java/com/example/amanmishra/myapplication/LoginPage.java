package com.example.amanmishra.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class LoginPage extends AppCompatActivity {
    private static final String TAG = "LoginPage";

    private VerifyUserExist mAuthTask = null;

    // Progress Dialog
    private ProgressDialog pDialog;

    private EditText mEmailView;
    private EditText mPasswordView;
    private TextView forgotPasswordButton;
    //private TextView changePasswordButton;
    private URL url;
    private HttpURLConnection conn;

    // JSON Node names
    private static final String TAG_SUCCESS = "success";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        forgotPasswordButton = (TextView) findViewById(R.id.forgot_pass_b);
        //changePasswordButton = (TextView) findViewById(R.id.change_pass_b);



        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        forgotPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgotPassword();
            }
        });
        //changePasswordButton.setOnClickListener(new View.OnClickListener() {
            //@Override
            //public void onClick(View view) {
            //    changePassword();
            //}
        //});
    }

    private void forgotPassword(){
        Intent forgotPass = new Intent(getApplicationContext(), ForgotPassword.class);
        startActivity(forgotPass);
    }

    //private void changePassword(){
    //    Intent changePass = new Intent(getApplicationContext(), ChangePassword.class);
    //    startActivity(changePass);
    //}


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }

        // Check for a valid password
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            // url to create new product
            try {
                url = new URL("http://ec2-54-67-34-214.us-west-1.compute.amazonaws.com/get_user_detials.php");
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestMethod("POST");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            String hashPassword = MD5(password);
            Log.d("LOGPAGE", hashPassword);
            if(hashPassword == null){
                mAuthTask = null;
                Toast.makeText(LoginPage.this, "Action unable to be completed securely. Please try again.", Toast.LENGTH_LONG).show();
                return;
            }
            mAuthTask = new VerifyUserExist(email, hashPassword);
            mAuthTask.execute();
        }
    }

    public String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            md.update(md5.getBytes());

            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    /**
     * Background Async Task to Create new user
     * */
    class VerifyUserExist extends AsyncTask<String, String, String> {
        private final String mEmail;
        private final String mPassword;
        private String response;

        VerifyUserExist(String mEmail, String mPassword) {
            this.mEmail = mEmail;
            this.mPassword = mPassword;
        }


        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginPage.this);
            pDialog.setMessage("Signing In...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating product
         */
        protected String doInBackground(String... args) {
            // getting JSON Object
            // Note that create product url accepts POST method
            try{
                HashMap<String, String> regData = new HashMap<>();
                regData.put("email", mEmail);
                regData.put("password", mPassword);

                // For POST only - BEGIN
                OutputStream os = conn.getOutputStream();
                BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                wr.write(getPostDataString(regData));
                wr.flush();
                wr.close();
                os.close();
                // For POST only - END

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) { //success
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    response = br.readLine();
                    br.close();

                    return "success";
                } else {
                    return "failure";
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return "failure2";
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            mAuthTask = null;
            pDialog.dismiss();
            if(file_url == "success") {
                try {
                    JSONObject obj = (JSONObject) new JSONTokener(response).nextValue();
                    int success = obj.getInt("success");
                    String msg = obj.getString("message");

                    Log.d("LogPage", msg);

                    if(success == 1){
                        SaveSharedPreference.setUserName(LoginPage.this, mEmail);
                        if(msg.equalsIgnoreCase("verify")){
                            Intent verification = new Intent(getBaseContext(), VerifyUser.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("email", mEmail);
                            verification.putExtras(bundle);
                            startActivity(verification);
                            finish();
                        }else{
                            Toast.makeText(LoginPage.this, msg, Toast.LENGTH_LONG).show();
                            Intent homescreen = new Intent(getBaseContext(), HomeScreen.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("email", mEmail);
                            homescreen.putExtras(bundle);
                            startActivity(homescreen);
                        }
                    }else if(msg.equalsIgnoreCase("password")){
                        mPasswordView.setError(getString(R.string.error_incorrect_password));
                        mPasswordView.requestFocus();
                    }else{
                        mEmailView.setError(getString(R.string.error_invalid_email));
                        mEmailView.requestFocus();
                    }

                } catch(JSONException e){
                    Log.d("Json exception", response);
                }
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            pDialog.dismiss();
        }

        private String getPostDataString(HashMap<String, String> postDataParams) throws UnsupportedEncodingException {
            StringBuilder result = new StringBuilder();
            boolean first = true;
            for(Map.Entry<String, String> entry : postDataParams.entrySet()){
                if (first){
                    first = false;
                }
                else {
                    result.append("&");
                }
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
            return result.toString();
        }
    }
}

