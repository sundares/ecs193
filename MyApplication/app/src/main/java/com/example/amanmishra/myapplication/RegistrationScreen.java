package com.example.amanmishra.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;


import static android.app.PendingIntent.getActivity;

public class RegistrationScreen extends AppCompatActivity {
    ProgressDialog regloading = null;

    private AutoCompleteTextView mFirstNameView;
    private AutoCompleteTextView mLastNameView;
    private AutoCompleteTextView mEmailView;
    private EditText mPassView;
    private EditText mPassDupView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        regloading  = new ProgressDialog(RegistrationScreen.this);
        regloading.setCancelable(true);
        regloading.setMessage("Registering");
        regloading.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        mFirstNameView = (AutoCompleteTextView) findViewById(R.id.fname);
        mLastNameView = (AutoCompleteTextView) findViewById(R.id.lname);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.reg_email);
        mPassView = (EditText) findViewById(R.id.reg_pass1);
        mPassDupView = (EditText) findViewById(R.id.reg_pass2);

        Button btnReg = (Button)findViewById(R.id.registration_button);
        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkRegistrationForm();
            }
        });
    }

    private void checkRegistrationForm() {
        mFirstNameView.setError(null);
        mLastNameView.setError(null);
        mEmailView.setError(null);
        mPassView.setError(null);
        mPassDupView.setError(null);

        //Store values at registration attempt
        String firstName = mFirstNameView.getText().toString();
        String lastName = mLastNameView.getText().toString();
        String email = mEmailView.getText().toString();
        String pass = mPassView.getText().toString();
        String passdup = mPassDupView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(firstName)){
            mFirstNameView.setError("This field is required");
            focusView = mFirstNameView;
            cancel = true;
        }
        else if (TextUtils.isEmpty(lastName)){
            mLastNameView.setError("This field is required");
            focusView = mLastNameView;
            cancel = true;
        }
        else if (TextUtils.isEmpty(email)){
            mEmailView.setError("This field is required");
            focusView = mEmailView;
            cancel = true;
        }
        else if (!isValidEmail(email)){
            mEmailView.setError("This email address is invalid");
            focusView = mEmailView;
            cancel = true;
        }
        else if (TextUtils.isEmpty(pass)){
            mPassView.setError("This field is required");
            focusView = mPassView;
            cancel = true;
        }
        else if (!TextUtils.equals(pass,passdup)){
            mPassDupView.setError("Passwords do not match");
            focusView = mPassDupView;
            cancel = true;
        }
        if (cancel) {
            focusView.requestFocus();
        }
        else{
            register(firstName, lastName, email, pass);
        }

    }

    private boolean isValidEmail(String email) {
        return email.contains("@");
    }

    private void register(String firstName, String lastName, String email, String pass){
        class RegisterUser extends AsyncTask<String, Void, String>{
            private static final String REGISTER_URL = "http://ec2-52-37-23-203.us-west-2.compute.amazonaws.com/DiaryStudies/register.php";
            RegisterLoginUserClass rluc = new RegisterLoginUserClass();

            @Override
            protected void onPreExecute() {
                regloading.show();
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params) {
                HashMap<String,String> data = new HashMap<>();
                data.put("firstname",params[0]);
                data.put("lastname",params[1]);
                data.put("email", params[2]);
                data.put("password", params[3]);
                String result = rluc.sendPostRequest(REGISTER_URL,data);
                return  result;
            }
            @Override

            protected void onPostExecute(String s) {
                regloading.dismiss();
                super.onPostExecute(s);
                Intent startLogin = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(startLogin);
                Toast.makeText(getApplication().getBaseContext(), s, Toast.LENGTH_LONG).show();

            }
        }
        RegisterUser ru = new RegisterUser();
        ru.execute(firstName, lastName,email, pass);
    }


}
