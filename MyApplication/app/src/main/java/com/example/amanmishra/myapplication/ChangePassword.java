package com.example.amanmishra.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class ChangePassword extends AppCompatActivity {
    // Progress Dialog
    private ProgressDialog pDialog;
    private ChangeNewPassword mAuthTask = null;

    private EditText mEmailView;
    private EditText mPasswordView;
    private EditText mPasswordNewV;
    private EditText mPasswordNewDup;

    private URL url;
    private HttpURLConnection conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.passwordOld);
        mPasswordNewV = (EditText) findViewById(R.id.passwordNew);
        mPasswordNewDup = (EditText) findViewById(R.id.passwordNewDup);

        Button forgotPasswordButton = (Button) findViewById(R.id.change_pass_b);
        forgotPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePassword();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // Handle "up" button behavior here.
            Bundle b = getIntent().getExtras();
            String email = b.getString("email");
                /*String firstName = b.getString("firstName");*/
            Intent usersettings = new Intent(getApplicationContext(), UserSettings.class);
            Bundle bundle = new Bundle();
            bundle.putString("email", email);
            usersettings.putExtras(bundle);
            startActivity(usersettings);
            return true;
        } else {
            // handle other items here
        }
        return true;// return true if you handled the button click, otherwise return false.
    }

    private void changePassword() {
        mEmailView.setError(null);
        mPasswordView.setError(null);
        mPasswordNewV.setError(null);
        mPasswordNewDup.setError(null);

        String email = mEmailView.getText().toString();
        String passwordOld = mPasswordView.getText().toString();
        String passwordNew = mPasswordNewV.getText().toString();
        String passwordNewDup = mPasswordNewDup.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }

        if (TextUtils.isEmpty(passwordOld)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(passwordNew)) {
            mPasswordNewV.setError(getString(R.string.error_field_required));
            focusView = mPasswordNewV;
            cancel = true;
        }else if(passwordNew.length() < 4){
            mPasswordNewV.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordNewV;
            cancel = true;
        } else if(!passwordNew.equals(passwordNewDup)){
            mPasswordNewV.setError("Passwords do not match.");
            mPasswordNewDup.setError("Passwords do not match.");
            focusView = mPasswordNewV;
            focusView = mPasswordNewDup;
            cancel = true;
        }

        if (TextUtils.isEmpty(passwordNewDup)) {
            mPasswordNewDup.setError(getString(R.string.error_field_required));
            focusView = mPasswordNewDup;
            cancel = true;
        }else if(passwordNewDup.length() < 4) {
            mPasswordNewDup.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordNewDup;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            // url to create new product
            try {
                url = new URL("http://ec2-54-67-34-214.us-west-1.compute.amazonaws.com/change_password.php");
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestMethod("POST");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            String hashOld = MD5(passwordOld);
            String hashNew = MD5(passwordNew);
            if(hashOld == null || hashNew == null){
                mAuthTask = null;
                Toast.makeText(ChangePassword.this, "Action unable to be completed securely. Please try again.", Toast.LENGTH_LONG).show();
                return;
            }
            mAuthTask = new ChangeNewPassword(email, hashOld, hashNew);
            mAuthTask.execute();
        }
    }

    public String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            md.update(md5.getBytes());

            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    class ChangeNewPassword extends AsyncTask<String, String, String> {
        private final String mEmail, mPassOld, mPassNew;
        private String response;

        ChangeNewPassword(String mEmail, String mPassOld, String mPassNew) {
            this.mEmail = mEmail;
            this.mPassOld = mPassOld;
            this.mPassNew = mPassNew;
        }


        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ChangePassword.this);
            pDialog.setMessage("Changing Password...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating product
         */
        protected String doInBackground(String... args) {
            // getting JSON Object
            // Note that create product url accepts POST method
            try{
                HashMap<String, String> regData = new HashMap<>();
                regData.put("email", mEmail);
                regData.put("passwordOld", mPassOld);
                regData.put("passwordNew", mPassNew);

                // For POST only - BEGIN
                OutputStream os = conn.getOutputStream();
                BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                wr.write(getPostDataString(regData));
                wr.flush();
                wr.close();
                os.close();
                // For POST only - END

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) { //success
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    response = br.readLine();
                    br.close();

                    return "success";
                } else {
                    return "failure";
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return "failure2";
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            mAuthTask = null;
            pDialog.dismiss();
            if(file_url == "success") {
                try {
                    JSONObject obj = (JSONObject) new JSONTokener(response).nextValue();
                    int success = obj.getInt("success");
                    String msg = obj.getString("message");

                    Log.d("LogPage", msg);

                    if(success == 1){
                        msg = "Password successfully changed! Please begin login process again";
                        Toast.makeText(ChangePassword.this, msg, Toast.LENGTH_LONG).show();
                        Intent loginAgain = new Intent(getApplicationContext(), LoginPage.class);
                        startActivity(loginAgain);
                    }else{
                        if(msg.equalsIgnoreCase("email")) {
                            View focusView = null;
                            mEmailView.setError("Invalid email password combination.");
                            mPasswordView.setError("Invalid email password combination.");
                            focusView = mEmailView;
                            focusView = mPasswordView;
                            focusView.requestFocus();
                        } else{
                            Toast.makeText(ChangePassword.this, msg, Toast.LENGTH_LONG).show();
                        }
                    }

                } catch(JSONException e){
                    Log.d("Json exception", response);
                }
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            pDialog.dismiss();
        }

        private String getPostDataString(HashMap<String, String> postDataParams) throws UnsupportedEncodingException {
            StringBuilder result = new StringBuilder();
            boolean first = true;
            for(Map.Entry<String, String> entry : postDataParams.entrySet()){
                if (first){
                    first = false;
                }
                else {
                    result.append("&");
                }
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
            return result.toString();
        }
    }
}
