package com.example.amanmishra.myapplication;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;


public class HomeScreen extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle b = getIntent().getExtras();
        email = b.getString("email");
        //String fname = b.getString("firstName");
        //String firstName = getIntent().getStringExtra("firstName");
        //String lname = b.getString("lastName");

        //TextView emailtext = (TextView) findViewById(R.id.email);
        //emailtext.setText(email);

        //TextView firsttext = (TextView) findViewById(R.id.firstName);
        //firsttext.setText(firstName);
/*
        Bundle b = getIntent().getExtras();
        String firstName = b.getString("firstName");
        TextView myname = (TextView) findViewById(R.id.firstName);
        myname.setText(firstName); */
/*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
/*
           TextView myname = (TextView)findViewById(R.id.firstName);
        myname.setText("My Awesome Text");
        */
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.home_screen);
        Button btn = new Button(getApplication().getBaseContext());
        btn.setText("Survey Info Page");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent info = new Intent(getApplicationContext(),SurveyInfoPage.class);
                info.putExtra("email", email);
                startActivity(info);
            }
        });
        rl.addView(btn);

        Button StartSurveyButton = (Button) findViewById(R.id.strtsrvbtn);
        StartSurveyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = getIntent().getExtras();
                String email = b.getString("email");
                Intent startSurvey = new Intent(getApplicationContext(), SurveyActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("email", email);
                startSurvey.putExtras(bundle);
                startActivity(startSurvey);
            }
        });
/*
        Button UserSettingsButton = (Button) findViewById(R.id.settingsbtn);
        UserSettingsButton.setOnClickListener(new View.OnClickListener() {
 /*           @Override
            public void onClick(View v) {
/*                Bundle b = getIntent().getExtras();
                String email = b.getString("email");
                /*String firstName = b.getString("firstName");*/
  /*              Intent userSettings = new Intent(getApplicationContext(), UserSettings.class);
                Bundle bundle = new Bundle();
                bundle.putString("email", email);
                userSettings.putExtras(bundle);
                startActivity(userSettings);
            }
        });

*/
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }//oncreate

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Bundle b = getIntent().getExtras();
                String email = b.getString("email");
                /*String firstName = b.getString("firstName");*/
                Intent userSettings = new Intent(getApplicationContext(), UserSettings.class);
                Bundle bundle = new Bundle();
                bundle.putString("email", email);
                userSettings.putExtras(bundle);
                startActivity(userSettings);
                // User chose the "Settings" item, show the app settings UI...
                //return true;
/*
            case R.id.strtsrvbtn:
                Bundle b1 = getIntent().getExtras();
                String email1 = b1.getString("email");

                Intent startSurvey = new Intent(getApplicationContext(), SurveyActivity.class);
                Bundle bundle1 = new Bundle();
                bundle1.putString("email", email1);
                startSurvey.putExtras(bundle1);
                startActivity(startSurvey);
*/
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        Bundle b = getIntent().getExtras();
        String email = b.getString("email");
        savedInstanceState.putString("email", email);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "HomeScreen Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.amanmishra.myapplication/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "HomeScreen Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.amanmishra.myapplication/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
