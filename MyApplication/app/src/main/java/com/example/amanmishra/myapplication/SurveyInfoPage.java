package com.example.amanmishra.myapplication;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

public class SurveyInfoPage extends AppCompatActivity {
    String email;
    RegisterLoginUserClass ids =new RegisterLoginUserClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        email = getIntent().getStringExtra("email");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_info_page);
        getSurveyInfo();
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        this.recreate();
    }

    private void getSurveyInfo(){
        class getSurveyTask extends AsyncTask<String, Void, String> {
            private static final String url = "http://ec2-54-67-34-214.us-west-1.compute.amazonaws.com/surveyInfo.php";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params){
                HashMap<String,String> data = new HashMap<>();
                data.put("email", email);
                String result = ids.sendPostRequest(url, data);
                return result;
            }

            @Override
            protected void onPostExecute(String s){
                super.onPostExecute(s);
                //Toast.makeText(getApplication().getBaseContext(), s, Toast.LENGTH_LONG).show();
                if(s == null){
                    Toast.makeText(SurveyInfoPage.this, "WiFi not connected. Please check connection before continuing.", Toast.LENGTH_LONG);
                    return;
                }
                Log.d("Survey IDS", s);
                parseSurveys(s);
            }
        }
        getSurveyTask getSurveys = new getSurveyTask();
        getSurveys.execute();
    }

    private void parseSurveys(String s) {
        TextView surveytext = (TextView) findViewById(R.id.surveyTextView2);
        if(s.equalsIgnoreCase("200")){
            surveytext.setText("WiFi not available. Please check connection before proceeding.");
            return;
        }
        if(s.equalsIgnoreCase("0 results")){
            surveytext.setText("No Surveys Available. Please add surveys on the home page by clicking on the setting button.");
            return;
        }
        String delims = "\\+";
        String[] tokens = s.split(delims);
        surveytext.setText("Enrolled Surveys:");
        LinearLayout ll = (LinearLayout) findViewById(R.id.surveylinearlayout2);
        for(int i=0; i< tokens.length; i++){
            TextView survey = new TextView(getApplication().getBaseContext());
            String[] tokens2 = tokens[i].split("\\s+");
            Log.d("INFO", tokens[i]);
            // Log.d("INFO", new String(tokens2.length));
            String s1 = "\nSurvey ID: " + tokens2[0] + "\u0009" + tokens2[2] + "-" + tokens2[3];
            String s2 = "\nStudy Name:" + tokens2[1] + "\u0009" + tokens2[4] + "-" + tokens2[5];
            String s3 = "\nComplete for the day:" + tokens2[6] + "\n";
            survey.setText(s1 + s2 + s3);

            ll.addView(survey);
        }
    }

}
