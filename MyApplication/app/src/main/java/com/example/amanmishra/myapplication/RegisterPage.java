package com.example.amanmishra.myapplication;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.creativityapps.gmailbackgroundlibrary.BackgroundMail;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class RegisterPage extends AppCompatActivity {
    private static final String TAG = "RegisterPage";

    private CreateNewUser mAuthTask = null;

    // Progress Dialog
    private ProgressDialog pDialog;

    private EditText mEmailView;
    private EditText mPasswordView;
    private EditText mPasswordDup;
    private EditText mFNameView;
    private EditText mLNameView;
    private URL url;
    private HttpURLConnection conn;

    // JSON Node names
    private static final String TAG_SUCCESS = "success";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_page);
        //ActionBar actionBar = getActionBar();
        //actionBar.setHomeButtonEnabled(true);

        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordDup = (EditText) findViewById(R.id.passwordDup);
        mFNameView = (EditText) findViewById(R.id.firstName);
        mLNameView = (EditText) findViewById(R.id.lastName);

        Button regButton = (Button) findViewById(R.id.register_button);
        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegister();
            }
        });
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptRegister() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);
        mPasswordDup.setError(null);
        mLNameView.setError(null);
        mFNameView.setError(null);

        // Store values at the time of the login attempt.
        String fName = mFNameView.getText().toString();
        String lName = mLNameView.getText().toString();
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String passwordDup = mPasswordDup.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid firstName.
        if (TextUtils.isEmpty(fName)) {
            mFNameView.setError(getString(R.string.error_field_required));
            focusView = mFNameView;
            cancel = true;
        } else if (fName.length() > 30) {
            mFNameView.setError(getString(R.string.error_30char));
            focusView = mFNameView;
            cancel = true;
        }

        // Check for a valid lastName.
        if (TextUtils.isEmpty(lName)) {
            mLNameView.setError(getString(R.string.error_field_required));
            focusView = mLNameView;
            cancel = true;
        } else if (lName.length() > 30) {
            mLNameView.setError(getString(R.string.error_30char));
            focusView = mLNameView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        // Check for a valid password
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if( !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(passwordDup)) {
            mPasswordDup.setError(getString(R.string.error_field_required));
            focusView = mPasswordDup;
            cancel = true;
        }  else if(!password.equals(passwordDup)){
            mPasswordView.setError("Passwords do not match.");
            mPasswordDup.setError("Passwords do not match.");
            focusView = mPasswordView;
            focusView = mPasswordDup;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            // url to create new product
            try {
                url = new URL("http://ec2-54-67-34-214.us-west-1.compute.amazonaws.com/create_user.php");
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            String hashPassword = MD5(password);
            if(hashPassword == null){
                mAuthTask = null;
                Toast.makeText(RegisterPage.this, "Action unable to be completed securely. Please try again.", Toast.LENGTH_LONG).show();
                return;
            }
            mAuthTask = new CreateNewUser(email, hashPassword, fName, lName);
            mAuthTask.execute();
        }
    }

    public String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            md.update(md5.getBytes());

            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        if(email.contains("@") && email.length() <= 45)
            return true;

        return false;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Background Async Task to Create new user
     * */
    class CreateNewUser extends AsyncTask<String, String, String> {
        private final String mEmail;
        private final String mPassword;
        private final String mFName;
        private final String mLName;

        private String response = " ";


        CreateNewUser(String mEmail, String mPassword, String mFName, String mLName) {
            this.mEmail = mEmail;
            this.mPassword = mPassword;
            this.mFName = mFName;
            this.mLName = mLName;
        }


        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(RegisterPage.this);
            pDialog.setMessage("Creating User..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating product
         */
        protected String doInBackground(String... args) {
            // getting JSON Object
            // Note that create product url accepts POST method
            try{
                HashMap<String, String> regData = new HashMap<>();
                regData.put("email", mEmail);
                regData.put("password", mPassword);
                regData.put("firstName", mFName);
                regData.put("lastName", mLName);


                // For POST only - BEGIN
                OutputStream os = conn.getOutputStream();
                BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                wr.write(getPostDataString(regData));
                wr.flush();
                wr.close();
                os.close();
                // For POST only - END

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) { //success
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    response = br.readLine();
                    Log.d("REGPAGE", response);
                    br.close();

                    return "success";
                } else {
                    return "failure";
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return "failure2";
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            mAuthTask = null;
            pDialog.dismiss();
            if(file_url == "success") {
                try {
                    JSONObject obj = (JSONObject) new JSONTokener(response).nextValue();
                    int success = obj.getInt("success");
                    String msg = obj.getString("message");

                    if(success == 1) { //User successfully created
                        SaveSharedPreference.setNames(RegisterPage.this, mFName, mLName);
                        Toast.makeText(RegisterPage.this, "User successfully created.", Toast.LENGTH_LONG).show();
                        TextView myname = (TextView) findViewById(R.id.firstName);
                        myname.setText(mFName);
                        finish();
                    }else if(msg.equalsIgnoreCase("email")) { //success = 0
                        mEmailView.setError(getString(R.string.error_email_duplicate));
                        mEmailView.requestFocus();
                    }else{ //failure to insert row?
                        Toast.makeText(RegisterPage.this, msg, Toast.LENGTH_LONG).show();
                        finish();
                    }
                }catch(JSONException e){
                    Log.d("Json exception", response);
                }
            } else {
                Toast.makeText(RegisterPage.this, file_url, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            pDialog.dismiss();
        }

        private String getPostDataString(HashMap<String, String> postDataParams) throws UnsupportedEncodingException {
            StringBuilder result = new StringBuilder();
            boolean first = true;
            for(Map.Entry<String, String> entry : postDataParams.entrySet()){
                if (first){
                    first = false;
                }
                else {
                    result.append("&");
                }
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
            return result.toString();
        }
    }
}

