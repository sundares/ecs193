package com.example.amanmishra.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class UserSettings extends AppCompatActivity {

    private Button changePasswordButton;
    private Button addSurveyID;
    private Button deleteSurvey;
    private Button logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle b = getIntent().getExtras();
        String email = b.getString("email");
        TextView emailtext = (TextView) findViewById(R.id.email);
        emailtext.setText(email);

        changePasswordButton = (Button) findViewById(R.id.change_pass_b);
        addSurveyID = (Button) findViewById(R.id.addSurveyID);
        deleteSurvey = (Button) findViewById(R.id.delSurveyID);
        logout = (Button) findViewById(R.id.Logout);

        changePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePassword();
            }
        });

        addSurveyID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNewID();
            }
        });

        deleteSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDelID();
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveSharedPreference.clearUserName(UserSettings.this);
                Intent startMainScreen = new Intent(getApplicationContext(), MainPage.class);
                startActivity(startMainScreen);
                finish();
            }
        });
/*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void changePassword(){
        Bundle b = getIntent().getExtras();
        String email = b.getString("email");
        b.putString("email", email);
        Intent changePass = new Intent(getApplicationContext(), ChangePassword.class);
        changePass.putExtras(b);
        startActivity(changePass);
    }

    private void setNewID() {
        Bundle b = getIntent().getExtras();
        String email = b.getString("email");
        /*String firstName = b.getString("firstName");*/
        Intent addID = new Intent(getApplicationContext(), NewSurveyID.class);
        Bundle bundle = new Bundle();
        bundle.putString("email", email);
        addID.putExtras(bundle);
        startActivity(addID);
    }

    private void setDelID() {
        Bundle b = getIntent().getExtras();
        String email = b.getString("email");
        /*String firstName = b.getString("firstName");*/
        Intent addID = new Intent(getApplicationContext(), DelSurveyActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("email", email);
        addID.putExtras(bundle);
        startActivity(addID);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // Handle "up" button behavior here.
            Bundle b = getIntent().getExtras();
            String email = b.getString("email");
                /*String firstName = b.getString("firstName");*/
            Intent homeScreen = new Intent(getApplicationContext(), HomeScreen.class);
            Bundle bundle = new Bundle();
            bundle.putString("email", email);
            homeScreen.putExtras(bundle);
            startActivity(homeScreen);
            return true;
        } else {
            // handle other items here
        }
        return true;// return true if you handled the button click, otherwise return false.
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        Bundle b = getIntent().getExtras();
        String email = b.getString("email");
        //TextView emailtext = (TextView) findViewById(R.id.email);
        //emailtext.setText(email);

        savedInstanceState.putString("email", email);
    }


}
