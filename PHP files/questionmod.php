<?php
  $randomize = array();
  
  if($_SERVER['REQUEST_METHOD']=='POST'){
    require_once('db.php');
	$surveyid = $_POST['survey'];
	
	$parts = mysqli_query($con, "SELECT surveyPart FROM `studyid_parts` WHERE studyIDUnique=$surveyid ORDER BY surveyPart");
    
    foreach($parts as $part){
		$sPart = $part["surveyPart"];
		$sql = "SELECT QuestionID,QuestionType,Question,Randomize FROM surveys WHERE SurveyID='$sPart' ORDER BY `surveys`.`QuestionID` ASC";	
		$result = mysqli_query($con, $sql);
		
		if($result->num_rows > 0){
		  $data = array();
		  while($row1 = $result->fetch_assoc()) {
			$data[] = $row1;
		  }
		  
		  foreach($data as $row1) {
			  if($row1["Randomize"] == 1){
				$randomize[] = $row1;
			  }
		  }
		  
		  shuffle($randomize);
			header('Content-Type: text/plain');	
		 //output data of each row
			foreach($data as $row) {
				if($row["Randomize"] == 1){
					$row1 = array_shift($randomize);
					echo $row1["QuestionID"], "^", $row1["QuestionType"], "^", $row1["Question"], "\n";
				}else{
					echo $row["QuestionID"], "^", $row["QuestionType"], "^", $row["Question"], "\n";		

				}
			}   
		} 
		else {
			echo "0 results";
		} 
	}
  } else{
    echo "error try again";
  }
?>
