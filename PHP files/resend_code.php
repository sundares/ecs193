<?php

include 'mailfunctions.php';

$response = array();

if (isset($_POST['email'])) {
	$email = $_POST['email'];
	
	// include db connect class
    require_once('db.php');
	
	$emailExists = mysqli_query($con, "SELECT * FROM `userinfo` WHERE email = '$email'");
	
	//email id does not exist -> cannot reset password
	if (mysqli_num_rows($emailExists) <= 0) {
		$response["success"] = 0;
		$response["message"] = "email";
		
		echo json_encode($response);
	}  else {
		
		//SEND EMAIL WITH NEW CODE
		$row = mysqli_fetch_array($emailExists);
		$code = $row["verificationCode"];
		
		$firstName = $row["firstName"];
		$lastName = $row["lastName"];
		$subject = "DailyDiaryStudy- Email Verification";
		$body = "Hello $firstName $lastName," . PHP_EOL . PHP_EOL . "\tWelcome to Daily Diary Studies! Please enter this verification code " 
			. "next time you log into the app." . PHP_EOL . PHP_EOL . "\tverification code: $code" 
			. PHP_EOL . PHP_EOL . " Regards," . PHP_EOL . "Daily Diary Team";
			
		$sent = sendMail($firstName, $email, $subject, $body);
		
		if($sent){
			$response["success"] = 1;
			$response["message"] = "sent";
		}else{
			$response["success"] = 0;
			$response["message"] = "Trouble with verification process. Please try again later.";
		}
		
		echo json_encode($response);
	}
	
} else {
	$response["success"] = 0;
	$response["message"] = "Required field(s) is missing";
	
	echo json_encode($response);
}

?>