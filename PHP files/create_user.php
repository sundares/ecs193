<?php
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */
 
// array for JSON response
$response = array();

include 'mailfunctions.php';

function generateRandomString($length = 10) {
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}
 
if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['firstName']) && isset($_POST['lastName'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
	$firstName = $_POST['firstName'];
	$lastName = $_POST['lastName'];
 
    // include db connect class
    require_once('db.php');
	
	// get a product from products table
	$emailExists = mysqli_query($con, "SELECT * FROM `userinfo` WHERE email = '$email'");
		
	//if studyID does not exist -> return
	if(mysqli_num_rows($emailExists) > 0){
		$response["success"] = 0;
		$response["message"] = "email";
		
		echo json_encode($response);
	}else{
		// mysql inserting a new row
		$vCode = generateRandomString();
		$result = mysqli_query($con, "INSERT INTO userinfo(email, password, verificationCode, firstName, lastName) VALUES('$email', '$password', '$vCode', '$firstName', '$lastName')");
 
		// check if row inserted or not
		if ($result) {
			// successfully inserted into database
			$response["success"] = 1;
			$response["message"] = "success";
			
			//send email to client
			$subject = "DailyDiaryStudy- Email Verification";
			$body = "Hello $firstName $lastName," . PHP_EOL . PHP_EOL . "\tWelcome to Daily Diary Studies! Please enter this verification code " 
				. "next time you log into the app." . PHP_EOL . PHP_EOL . "verification code: $vCode" 
				. PHP_EOL . PHP_EOL . " Regards," . PHP_EOL . "Daily Diary Team";
				
			$sent = sendMail($firstName, $email, $subject, $body);
			if($sent){
				$response["success"] = 1;
				$response["message"] = "sent";
			}else{
				$response["success"] = 0;
				$response["message"] = "Email with login credential verification did not send. Please check email validity or try again later.";
			}
		} else {
			// failed to insert row
			$response["success"] = 0;
			$response["message"] = "System is down. Please try again later.";
		}
		
		// echoing JSON response
		echo json_encode($response);
	}
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>