function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}


function addRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;		
	if(rowCount){							// limit the user from creating fields more than your limits
		var row = document.createElement('tr'); // create row node      	
      	var td = document.createElement("TD");
        var text = document.createElement("INPUT");
        text.setAttribute("type", "text");
        text.setAttribute("required", "true");
        text.setAttribute("name", "opt[]");
        td.appendChild(text);
      	row.appendChild(td);
      	table.appendChild(row);

	}else{
		alert("Maximum Questions per survey is ?.");
			   
	}
}

function deleteRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	if(rowCount <= 1) { 						// limit the user from removing all the fields
		alert("Cannot Remove all the Options.");	
	}
	else{
		table.deleteRow(rowCount-1);
	}							
}


function dropcheck(elem){                
    var value = elem.value;
    if (value==2){
        // elem.nextElementSibling.style.display='';
        var breakline = document.createElement("br");
        elem.parentNode.appendChild(breakline);
        var label = document.createElement("LABEL");
        label.innerHTML = "Options";
        elem.parentNode.appendChild(label);
        var table = document.createElement("TABLE");
        table.setAttribute("id", "opttable");
        elem.parentNode.appendChild(table);
        
        var m = document.createElement("tbody");
        document.getElementById("opttable").appendChild(m);
        
        var y = document.createElement("TR");
        y.setAttribute("id", "myTr");
        document.getElementById("opttable").appendChild(y);

        var td = document.createElement("TD");
        var text = document.createElement("INPUT");
        text.setAttribute("type", "text");
        text.setAttribute("required", "true");
        text.setAttribute("name", "opt[]");
        td.appendChild(text);
        document.getElementById("myTr").appendChild(td);

        var addbtn = document.createElement("INPUT");
        addbtn.setAttribute("type", "button");
        addbtn.setAttribute("value", "Add Option");
        addbtn.setAttribute("onClick", "addRow('opttable')");

        var delbtn = document.createElement("INPUT");
        delbtn.setAttribute("type", "button");
        delbtn.setAttribute("value", "Delete Option");
        delbtn.setAttribute("onClick", "deleteRow('opttable')");


        elem.parentNode.appendChild(addbtn);
        elem.parentNode.appendChild(delbtn);        

    }
    else{
        while(elem.nextElementSibling){
        	elem.nextElementSibling.parentNode.removeChild(elem.nextElementSibling);
        }                    
    }
};

function addRowHelp(elem){
    addRow(elem.previousElementSibling);
}



function submitForm(action)
{
    document.getElementById('questionForm').action = action;
    document.getElementById('questionForm').submit();
}


function checkID(id){
	if (id==""){
		window.alert("Please fill in the Study ID");
		return;
	}
	else{
		var url = "./checkID.php?id=";	
		url=url.concat(id);
		window.open(url)
	}
	
}


function populateQuestionTable(tableID){
	var table = document.getElementById(tableID);	
	for (var i = 0, row; row = table.rows[i]; i++) {
   		if (i==0){
   			continue;
   		}
   		var td = document.createElement("TD");
      var button = document.createElement("BUTTON");
      button.setAttribute("name", "modQuest");
      button.innerHTML="Modify/Delete";
      button.setAttribute("value", i-1);
      button.setAttribute("type", "submit");
      // var callback ="document.getElementById(" + i +")";
      // callback = "alert("+ callback +")";  
      // button.setAttribute("onClick", callback); 
      td.appendChild(button);
      row.appendChild(td);       
   		//iterate through rows
   		//rows would be accessed using the "row" variable assigned in the for loop
   		// for (var j = 0, col; col = row.cells[j]; j++) {
     // 		//iterate through columns
     // 		//columns would be accessed using the "col" variable assigned in the for loop
   		// }  
	}
}

function setSelected(optionNum){
  var optionID = "opt"+optionNum;  
  var option = document.getElementById(optionID);
  option.setAttribute("selected", "selected");
  dropcheck(option.parentNode);
}

function setOptions(string, first){
  if (first==1){
    document.getElementsByName("opt[]")[0].value = string;
  }
  else{
    // var nodearr = document.getElementsByName("opt[]");
    // var parentNode = nodearr[0].parentNode;
    table = document.getElementById('opttable');
    var row = document.createElement('tr'); // create row node        
    var td = document.createElement("TD");
    var text = document.createElement("INPUT");
    text.setAttribute("type", "text");
    text.setAttribute("required", "true");
    text.setAttribute("name", "opt[]");
    text.value=string;
    td.appendChild(text);
    row.appendChild(td);
    table.appendChild(row);    
  }
}