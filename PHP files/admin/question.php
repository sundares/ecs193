<html>
    <head>
        <title>DiaryStudies Control Panel</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="js/scripts.js"></script>
    </head>
    <body>    
        <form action="next.php" class="register" method="POST">
            <h1>DiaryStudies Control Panel- Add Survey</h1>
			<?php 
            session_start();
            if(isset($_GET)==true && empty($_SESSION)==false): 
				print_r($_SESSION);
				$studyID = $_SESSION["studyID"];
				$verifCode = $_SESSION["verifCode"];
				$length = $_SESSION["length"];

			?>
			<fieldset class="row1">
                <legend>Survey Information</legend>
				<p>
                    <label>Study ID</label>
                    <input name="id" type="text" readonly="readonly" value="<?php echo $studyID ?>"/>
                </p>
                <p>
                	<label>Verification code</label>
					<input name= "verif" type="text" readonly="readonly" value="<?php echo $verifCode ?>"/>                             				
                </p>                    
				<div class="clear"></div>
            </fieldset>

            <fieldset>
            	<legend> Question </legend>
            		<table id="dataTable" class="form" border="1">                  		
                    		<tr>
                      			<p>
									<td>
										<label>Question Text</label>
										<input type="text" required="required" name="qText">
						 			</td>
                          			<td>
										<label for="qtype">Question Type</label>
											<select id="qType" name="qType" required="required" onchange="dropcheck(this)" onselect="dropcheck(this)">
												<option value = "0" selected="selected"> Slider </option>
												<option value = "1"> Free Response</option>
												<option value = "2"> Checkbox </option>
											</select>
                            			<div style='display:none;' id='options'>Options <br/>
                                			<br/>
                                			<table id="opttable">
                                    			<td> <input type="text" required="required" name="opt[]"></input> </td>
                                			</table>
                                			<input type='button'  value="Add Option" onClick="addRow('opttable')" />
                                			<input type="button" value="Remove Option" onclick= "deleteRow('opttable')"> </input>
                                			<br/>
                            			</div>
						 			</td>
								</p>
                    		</tr>                   		
                	</table>            	
            </fieldset>

            <fieldset class="row5">
                <legend>Submit</legend>
                <p>
					<input class="submit" type="submit" value="Add Question &raquo;" />					
                </p>
				<div class="clear"></div>
            </fieldset> 
		<?php else: ?>
		<fieldset class="row1">
			<legend>Sorry</legend>
			 <p>Some things went wrong please try again.</p>
		</fieldset>
		<?php endif; ?>
			<div class="clear"></div>
        </form>
    </body>
</html>





