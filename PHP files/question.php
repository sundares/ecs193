<?php
  if ($_SERVER['REQUEST_METHOD']=='POST'){
    require_once('db.php');
    $surveyid = $_POST['survey'];
      
      $sql = "SELECT QuestionID,QuestionType,Question FROM surveys WHERE SurveyID='".mysqli_real_escape_string($con, $surveyid)."'";
      
      //$check = mysqli_fetch_array(mysqli_query($con,$sql));
    
    $result = mysqli_query($con, $sql);
    
    class Question{
      public $type;
      public $text;
    }
    
    $questionList = array();
    
    if($result->num_rows > 0){
      while($row = $result->fetch_assoc()) {
        $tempQuestion = new Question();
        $tempQuestion->type = $row["QuestionType"];
        $tempQuestion->text = $row["Question"];
        $questionList[$row["QuestionID"]] = $tempQuestion;
      }
      ksort($questionList);
      echo json_encode($questionList);
      
    } else {
      echo "0 results";
    }
  } else {
    echo "Error, try again.";
  }
?>